    /*Parametrs of Field*/
const fieldWidth = 600; //px
const fieldHeight = 600; //px

    /*Parametrs of ball*/
const ballWidth = 50; //px
const ballSpeed = 5; //px
    
    /*Parametrs of wall*/
const wallWidth = 50; //px
const wallHeight = fieldHeight - ballWidth * 2; //px
const wallX = fieldWidth / 2 - wallWidth /2;
const wallY = - ballWidth;

    /*Styling field*/
$("#gameField").css("width", fieldWidth + "px");
$("#gameField").css("height", fieldHeight + "px");
  
    /*Styling ball*/
$("#ball").css("width", ballWidth + "px");
$("#ball").css("height", ballWidth + "px");
$("#ball").css("borderRadius", ballWidth + "px");

/*Styling wall*/
$("#wall").css("width", wallWidth + "px");
$("#wall").css("height", wallHeight + "px");
$("#wall").css("left", wallX + "px");
$("#wall").css("top", wallY + "px");


    /*Move the ball to the specified coordinates*/
function moveBall(ballX, ballY){

    if (ballX < 0)
        ballX = 0;  // Margin Left
    if (ballY < 0)
        ballY = 0;  // Margin Top
    if (ballX > fieldWidth - ballWidth)
        ballX = fieldWidth - ballWidth;  // Margin Right
    if (ballY > fieldHeight - ballWidth)
        ballY = fieldHeight - ballWidth;  // Margin Bottom
        
        // Set Position
    $("#ball").css("left", ballX + "px");
    $("#ball").css("top", ballY + "px");
}
    /*Check Colision Ball and Wall*/
function checkCollision(ballX, ballY){
    var XColl=false;
    var YColl=false;
    
    if ((ballX + ballWidth >= wallX) && (ballX <= wallX + wallWidth)) XColl = true;
    if ((ballY + ballWidth >= wallY) && (ballY <= wallY + wallHeight)) YColl = true;
  
    if (XColl&YColl){return false;}
    return true;
}

    /*Control the ball with arrows*/
$( document ).keydown(function( event ) {
    var key = event.which;
    switch(key) {
        case 38:
            ballX = $("#ball").position().left;
            ballY = $("#ball").position().top - ballSpeed;
            if (checkCollision(ballX, ballY)) 
                moveBall(ballX, ballY);  // UP
            break;
        case 39:
            ballX = $("#ball").position().left + ballSpeed;
            ballY = $("#ball").position().top;
            if (checkCollision(ballX, ballY)) 
                moveBall(ballX, ballY);  // Right
            break;
        case 40:
            ballX = $("#ball").position().left;
            ballY = $("#ball").position().top + ballSpeed;
            if (checkCollision(ballX, ballY)) 
                moveBall(ballX, ballY);  // Down
            break;
        case 37:
            ballX = $("#ball").position().left - ballSpeed;
            ballY = $("#ball").position().top;
            if (checkCollision(ballX, ballY)) 
                moveBall(ballX, ballY);  // Left
            break;
    }

});

    /*Control the ball with mouse*/
/*    
$( document ).on( "click", function( event ) {
    ballX = event.pageX - ballWidth/2;
    ballY = event.pageY - ballWidth/2;
    moveBall(ballX, ballY); 
});
*/
